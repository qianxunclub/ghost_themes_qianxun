
预览地址：http://qianxunclub.com/


# 使用说明

## 一、准备：
1、畅言插件：该主题使用了畅言的评论、热批、赞赏等功能，畅言官网：http://changyan.kuaizhan.com/

2、下载主题，放在ghost安装目录：```/home/app/ghost/content/themes```，下载地址：https://gitee.com/qianxunclub/ghost_themes_qianxun

3、百度统计，或者其他统计也行

## 二、底部信息修改
博客底部备案号修改，在themes_qianxun主题目录下的```default.hbs```文件修改，找到该文件，第```57```行，内容为：
```
<p>Copyright &copy; {{date format="YYYY"}} <a target="_blank" href="{{@blog.url}}">{{@blog.title}}</a> <a target="_blank" href="http://www.miitbeian.gov.cn">粤ICP备16022296号</a></p>
```
修改为自己的底部信息或者自定义样式。

## 三、配置方式

所有配置均在ghost后台```code injection```菜单下配置，配置分为两部分，```Blog Header```和```Blog Footer```

### Blog Header部分
```
<!-- 百度统计 -->
<script>
var _hmt = _hmt || [];
(function() {
  var hm = document.createElement("script");
  hm.src = "//hm.baidu.com/hm.js?3b92a7069a5181654fa7bf4e9b674c87";
  var s = document.getElementsByTagName("script")[0]; 
  s.parentNode.insertBefore(hm, s);
})();
</script>


<!-- 代码高亮 -->
<script src="http://cdn.bootcss.com/highlight.js/8.0/highlight.min.js"></script>
<link href="http://cdn.bootcss.com/highlight.js/8.0/styles/monokai_sublime.min.css" rel="stylesheet">


<!-- 标签云全剧函数 -->
<script type="text/javascript">
    var word_array = [];
</script>   

```

### Blog Footer部分
```
<script>
	/* 使用AJAX主题的时候需以下代码 */
	
	var ass = document.getElementsByTagName("a");
	for(var i=0;i<ass.length;i++){
		ass[i].setAttribute("target","_top");
	}


	/* 代码高亮 */
    hljs.initHighlightingOnLoad();


	/* 标签云 */
    var settings = {
			entries: word_array,
			width: '100%',
			height: '100%',
			radius: '75%',
			radiusMin: 75,
			bgDraw: false,
			opacityOver: 1.00,
			opacityOut: 0.1,
			opacitySpeed: 6,
			fov: 800,
			speed: 0.4,
			fontFamily: 'Oswald, Arial, sans-serif',
			fontSize: '16',
			fontColor: '#009688',
			fontWeight: 'normal',
			fontStyle: 'normal',
			fontStretch: 'normal'
		};
	$( '#tag-cloud' ).svg3DTagCloud( settings );

</script>


<!-- 评论数量，修改为自己畅言地址 -->
<script id="cy_cmt_num" src="http://changyan.sohu.com/upload/plugins/plugins.list.count.js?clientId=cysusDfMu"></script>


<script type="text/javascript"> 


	/* 畅言评论，修改为自己畅言地址 */
    if($("#SOHUCS").html() || $("#SOHUCS").html()==""){
        (function(){ 
            var appid = 'cysusDfMu'; 
            var conf = 'prod_b69f2ef223aee522c55cdfb59eb9a463'; 
            var width = window.innerWidth || document.documentElement.clientWidth; 
            if (width < 960) { 
                window.document.write('<script id="changyan_mobile_js" charset="utf-8" type="text/javascript" src="http://changyan.sohu.com/upload/mobile/wap-js/changyan_mobile.js?client_id=' + appid + '&conf=' + conf + '"><\/script>'); } else { var loadJs=function(d,a){var c=document.getElementsByTagName("head")[0]||document.head||document.documentElement;var b=document.createElement("script");b.setAttribute("type","text/javascript");b.setAttribute("charset","UTF-8");b.setAttribute("src",d);if(typeof a==="function"){if(window.attachEvent){b.onreadystatechange=function(){var e=b.readyState;if(e==="loaded"||e==="complete"){b.onreadystatechange=null;a()}}}else{b.onload=a}}c.appendChild(b)};loadJs("http://changyan.sohu.com/upload/changyan.js",
            function(){window.changyan.api.config({appid:appid,conf:conf})}); } })();
    }


	/* 公告，修改为自己的公告内容 */
    if($("#gonggao").html() || $("#gonggao").html()=="" ){
        var myText = "本人QQ：960339491，本博客使用ghost制作，欢迎有兴趣的朋友加入分享与交流经验。";
		var downLoad = "<br/><a target='_Blank' href='http://qianxunclub.com/ghostzhu-ti-qianxun-themes/'>下载本站</a>";
		var sendMail = "<br/><a target='_blank' href='http://mail.qq.com/cgi-bin/qm_share?t=qm_mailme&email=nqeorq2tp6qnr97v77D98fM' style='text-decoration:none;'><img src='http://rescdn.qqmail.com/zh_CN/htmledition/images/function/qm_open/ico_mailme_01.png'/></a>";
        
		var gonggaoText = myText + downLoad + sendMail;
		$("#gonggao").html(gonggaoText);
    }


	/* 友情链接，需要修改自己的友情连接 */
    if($("#youqinglianjie").html() || $("#youqinglianjie").html()=="" ){
        var youqinglianjie = "" + 
            "<li><a target='_Blank' href='http://qianxunclub.com/'>JAVA技术分享 - 千寻啊千寻</a></li>" + 
            "<li><a target='_Blank' href='http://yyzhangtian.com/'>冲锋剧团演员 - 演员张甜</a></li>";
        $("#youqinglianjie").html(youqinglianjie)
    }


	/* 添加搜索 */
    $(document).one('opening', '.remodal', function () {
    	$('#search').gsearch();
	});

</script>
```

# 效果展示

## PC端效果
<div  align="center">

### 首页效果
<img src="http://oh6t6o35a.bkt.clouddn.com/ghost_index.png" width=500/>
</div>

<div  align="center">

### 文章效果
<img src="http://oh6t6o35a.bkt.clouddn.com/ghost_act.png" width=500/>
</div>

## 手机端效果
<div  align="center">

### 首页效果
<img src="http://oh6t6o35a.bkt.clouddn.com/ghost_index_mob.png" width=500/>
</div>

<div  align="center">

### 文章效果
<img src="http://oh6t6o35a.bkt.clouddn.com/ghost_act_mob.png" width=500/>
</div>